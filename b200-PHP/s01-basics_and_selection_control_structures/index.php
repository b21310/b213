<?php require_once "./code.php" ?>
<?php require_once "./activity.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<!-- ACTIVITY -->
<p><?php echo getLetterGrade(87) ?></p>
    

    <!-- <h1>Echoing values!</h1> -->
    
    <!-- <p><?php echo "Good day $name! Your given email is $email."; ?></p> -->
    <!-- <p><?php echo PI; ?></p> -->
    <!-- <p><?php echo "The value of Pi is.",PI;?></p> -->
    <!-- (.) means to concatenate -->
    <!-- <p><?php echo "The value of Pi is " . PI .  ".";?></p> -->
    <!-- <p><?php echo gettype($hasTravelledAbroad) ?></p> -->
    <!-- <p><?php echo var_dump($hasTravelledAbroad) ?></p> -->
    <!-- <p><?php echo "MIX $mix" ?></p> -->

    <!-- Accessing array -->
    <!-- <p><?php echo $grades[1]; ?></p> -->

    <!-- Accessing objects -->
    <!-- <p><?php echo $gradesObj->firstGrading; ?></p> -->
    <!-- <p><?php echo $personObj->address->state; ?></p> -->

    <!-- Operators -->
    <!-- <p>X: <?php echo $x ?></p> -->
    <!-- <p>Y: <?php echo $y ?></p> -->

    <!-- Arithmetic Operators-->
    <!-- <p><?php echo $x + $y; ?></p> -->
    <!-- <p><?php echo $x - $y; ?></p> -->
    <!-- <p><?php echo $x * $y; ?></p> -->
    <!-- <p><?php echo $x / $y; ?></p> -->

    <!-- Equality Operators -->
    <!-- <p><?php echo var_dump($x == "123.14") ?></p> -->
    <!-- <p><?php echo var_dump($x === "123.14") ?></p> -->
     <!-- <p><?php echo var_dump($x != "123.14") ?></p> -->
        <!-- <p><?php echo var_dump($x !== "123.14") ?></p> -->

        <!-- Function -->
    <!-- <p>fullName: <?php echo getFullName("John", "Smith") ?></p> -->
 
</body>
</html>