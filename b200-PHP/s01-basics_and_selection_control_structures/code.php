<?php

// Variables
$name = "John Smith";
$email = "johnsmith@email.com";

//Contants
//-Defined using define() function. First param = name of constant, second param = value of param
define("PI", 3.53);

// Data types
//String
$state = "New York";
$country = "USA";

// Integers
$age = 31;
$headcount = 27;

// Boolean
$hasTravelledAbroad = true;

// Array - You can only have one data type inside an array
$grades = array(90.2,29.2,34.2);

// Objects. (=>) = assigning values in object
$gradesObj = (object)[
    "firstGrading" => 92.4,
    "secondGrading" => 23.2
];

$personObj = (object)[
    "fullName" => "John Smith",
    "isMarried" => false,
    "address" => (object)[
        "state" => "New York",
        "country" => "USA"
    ]
];

$x = 123.14;
$y = 1234.23;

// Function
function getFullName($firstName, $lastName){
    return "$firstName, $lastName";
};

// Switch statement
function determineComputerUser($computeNumber){
    switch($computeNumber){
        case 1:
            return "Linus";
            break;
    }
}