<?php

function whileLoop(){
    $count = 5;

    while($count !== 0){
        echo $count . "<br/>";
        $count--;
    }
}

function doWhileLoop(){
    $count = 20;

    do{
        echo $count . "<br/>";
        $count--;
    } while($count > 0);
}


$grades = [];